import React, { useState } from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./App.css";
import Sidebar from "./components/Sidebar";
import Todo from "./components/Todo";
import { Breadcrumb, Layout, Menu, theme, Badge } from "antd";
import { MessageFilled, MessageOutlined } from "@ant-design/icons";
import Projects from "./components/Projects";
import Comments from "./components/Comments";
import Labels from "./components/Labels";
const { Header, Content, Footer, Sider } = Layout;
function App() {
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  return (
    <>
      <BrowserRouter>
        <Layout>
          <Header style={{ background: "white", width: "100vw" }}></Header>
          <Content
            style={{
              padding: "0 ",
            }}
          >
            <Layout
              style={{
                padding: " 0",
                background: colorBgContainer,
                borderRadius: borderRadiusLG,
              }}
            >
              <Sider
                style={{
                  background: colorBgContainer,
                }}
              >
                <Sidebar />
              </Sider>
              <Content
                style={{
                  padding: "0 24px",
                  minHeight: 280,
                  minWidth: 280,
                  maxWidth: '100vw',
                  overflowX:'hidden',
                }}
              >
                <Routes>
                  <Route path="/" element={<Todo />} />
              <Route path="/labels" element={<Labels />} />
                  <Route path="/projects/:id" element={<Projects />} />
                </Routes>
              </Content>
            </Layout>
          </Content>
        </Layout>
      </BrowserRouter>
    </>
  );
}

export default App;
