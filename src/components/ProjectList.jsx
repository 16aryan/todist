import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Space, Checkbox, message } from "antd";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import Add from "./Add";
import { add, close, remove,updated } from "./Store/Slicer/todo";
import { addTodoSection, deleteTodo, completedTodo, updateTaskDetails } from "../API/api";
import Update from "./Update";

function ProjectList({ id }) {
  const sectionTasks = useSelector((state) =>
    state.todo.todo.filter(
      (todo) => todo.section_id !== null && todo.section_id === id
    )
  );
  const dispatch = useDispatch();

  const handleAddTask = async (data, date) => {
    try {
      const response = await addTodoSection(data, date, id);
      dispatch(add(response));
    } catch (error) {
      console.log("error in posting todo", error);
    }
  };

  const handleDelete = async (id) => {
    try {
      await deleteTodo(id);
      dispatch(remove(id));
    } catch (error) {
      console.log("error in deleting todo", error);
    }
  };

  const completedTask = async (id) => {
    try {
      await completedTodo(id);
      dispatch(close(id));
    } catch (error) {
      console.log("error in closed task:", error);
    }
  };

  const updateTask = async (id,data,date) => {
    try {
      const response = await updateTaskDetails(id,data,date);
      dispatch(updated({data,date,id}));
      
    } catch (error) {
      console.log("error in closed task:", error);
    }
  };

  return (
    <Space direction="vertical" style={{ marginTop: "2vw", cursor: "pointer" }}>
      {sectionTasks.length > 0 ? (
        sectionTasks.map((todo) => (
          <div
            key={todo.id}
            style={{
              width: "45vw",
              display: "flex",
              justifyContent: "space-between",
              padding: "20px",
            }}
          >
            <div style={{ cursor: "pointer", fontSize: "18px" }}>
              <Checkbox
                className="foo"
                onChange={() => completedTask(todo.id)}
                style={{ paddingRight: "20px" }}
              />
              {todo.content}
            </div>

            <div
              style={{
                width: "10vw",
                display: "flex",
                justifyContent: "space-around",
              }}
            >
            <Update  name={"task"} addfunction={updateTask} id={todo.id}/>
              <DeleteOutlined onClick={() => handleDelete(todo.id)} />
            </div>
          </div>
        ))
      ) : (
        <div style={{padding:'20px'}}>No tasks found.</div>
      )}
      <Add name={"task"} addfunction={handleAddTask} />
    </Space>
  );
}

export default ProjectList;
