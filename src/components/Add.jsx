import React, { useState } from "react";
import { Button, Form, Input, Modal, DatePicker } from "antd";

const Add = ({ name, addfunction }) => {
  const [open, setOpen] = useState(false);
  const [formData, setFormData] = useState("");
  const [date, setFormDate] = useState("");

  const handleCancel = () => {
    setOpen(false);
  };

  const onFinish = (values) => {
    
    setOpen(false);

    if (name === "task") {
      const dateObject = date.$d;
      const options = { day: "numeric", month: "short" };
      const dateString = dateObject.toLocaleDateString("en", options);

      addfunction(formData, dateString);
    } else {
      addfunction(formData);
    }
    setFormData("");

  };

  return (
    <>
      <Modal
        title={`Add ${name}`}
        open={open}
        onCancel={handleCancel}
        footer={null}
      >
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={formData}
          onFinish={onFinish}
        >
          <Input
            placeholder={`${name} name`}
            value={formData}
            onChange={(e) => setFormData(e.target.value)}
            required
          />
          {name === "task" ? (
            <DatePicker
              renderExtraFooter={() => "extra footer"}
              onChange={(date, dateString) => setFormDate(date)}
              required
            />
          ) : null}

          <Button
            type="primary"
            htmlType="submit"
            style={{ backgroundColor: "rgb(170,44,23)" }}
          >
            Submit
          </Button>
        </Form>
      </Modal>

      <Button
        type="primary"
        onClick={() => setOpen(true)}
        style={{ backgroundColor: "rgb(170,44,23)" }}
      >
        Add {name}
      </Button>
    </>
  );
};

export default Add;
