import React, { useEffect, useState } from "react";
import {
  PlusOutlined,
  MailOutlined,
  ProjectOutlined,
  DeleteOutlined,
  FilterOutlined,
  SearchOutlined,
  EditOutlined,
} from "@ant-design/icons";
import { Space, Card, message, Menu, Spin, Flex } from "antd";
import {
  fetchProject,
  deleteProject,
  addProject,
  updateProject,
} from "../API/api";
import { useSelector, useDispatch } from "react-redux";
import {
  get,
  remove,
  add,
  saveId,
  update,
} from "./Store/Slicer/slidebarSlicer";
import { Link } from "react-router-dom";
import Add from "./Add";
import { useNavigate, useLocation } from "react-router-dom";
import Update from "./Update";

const { SubMenu } = Menu;

function Sidebar() {
  const navigate = useNavigate();
  const location = useLocation();

  const [messageApi, contextHolder] = message.useMessage();
  const dispatch = useDispatch();
  const projects = useSelector((state) => state.projects.projects);
  const loading = useSelector((state) => state.projects.loading);
  const currentProjectId = location.pathname.split("/")[2];

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetchProject();
        dispatch(get({ response }));
        messageApi.open({
          type: "success",
          content: "Projects fetched successfully",
        });
      } catch (error) {
        console.error("Error fetching projects:", error);
        messageApi.open({
          type: "error",
          content: "Error fetching projects",
        });
      }
    };
    fetchData();
  }, [messageApi, dispatch]);

  const handleDelete = async (id) => {
    try {
      await deleteProject(id);
      dispatch(remove(id));
      messageApi.open({
        type: "success",
        content: "Project deleted successfully",
      });
    } catch (error) {
      console.error("Error deleting project:", error);
      messageApi.open({
        type: "error",
        content: "Error deleting project",
      });
    }
  };

  const handleAddProject = async (data) => {
    try {
      const response = await addProject(data);
      dispatch(add(response));
      messageApi.open({
        type: "success",
        content: "Project added successfully",
      });
    } catch (error) {
      console.error("Error adding project:", error);
      messageApi.open({
        type: "error",
        content: "Error adding project",
      });
    }
  };

  const updatingProject = async (projectId, data) => {
    try {
      const response = await updateProject(projectId, data);

      dispatch(update({ id: projectId, response: response }));
      messageApi.open({
        type: "success",
        content: "Project updated successfully",
      });
    } catch (error) {
      console.log("error updating project:", error);
      messageApi.open({
        type: "error",
        content: "Error updating project",
      });
    }
  };
  return (
    <>
      {contextHolder}
      <div style={{ width: 350, height: "100vh" }}>
        <Menu
          defaultSelectedKeys={["1"]}
          defaultOpenKeys={["sub1"]}
          mode="inline"
        >
          <Menu.Item key="add-project" icon={<PlusOutlined />}>
            <Add name={"projects"} addfunction={handleAddProject} />
          </Menu.Item>
          <Link to={`/labels`} >
            <Menu.Item key="filter" icon={<FilterOutlined />}style={{paddingLeft:'24px'}}>
              Filter and labels
            </Menu.Item>
          </Link>
          <Menu.Item key="search" icon={<SearchOutlined />}>
            Search
          </Menu.Item>
          <SubMenu key="sub1" icon={<ProjectOutlined />} title="Projects">
            {loading ? (
              <Flex align="center" justify="center">
                <Spin size="large" />
              </Flex>
            ) : (
              projects.map((project) => (
                <Menu.Item key={project.id} icon={<MailOutlined />}>
                  <Space direction="horizontal">
                    <Card
                      bordered={false}
                      style={{
                        background: "none",
                        color: "grey",
                        width: "10vw",
                      }}
                    >
                      <Link
                        to={`/projects/${project.id}`}
                        style={{ textDecoration: "none" }}
                      >
                        <div onClick={() => dispatch(saveId(project.id))}>
                          {project.name}
                        </div>
                      </Link>
                    </Card>

                    <DeleteOutlined
                      onClick={() => {
                        if (currentProjectId === project.id) {
                          navigate(-1);
                        }
                        handleDelete(project.id);
                      }}
                    />
                    <Update
                      name={"project"}
                      addfunction={updatingProject}
                      id={project.id}
                    />
                  </Space>
                </Menu.Item>
              ))
            )}
          </SubMenu>
        </Menu>
      </div>
    </>
  );
}

export default Sidebar;
