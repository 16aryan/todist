import { Card, Space, Checkbox, Flex, Spin, message } from "antd";
import React, { useEffect, useState } from "react";
import { DeleteOutlined } from "@ant-design/icons";
import { completedTodo, fetchTodo, deleteTodo, addTodo,updateTaskDetails } from "../API/api";
import { useDispatch, useSelector } from "react-redux";
import { get, close, remove, add,updated } from "./Store/Slicer/todo";
import Icon from "@ant-design/icons/lib/components/Icon";
import Add from "./Add";
import "../App.css"
import Update from "./Update";
function Todo() {
  const [messageApi, contextHolder] = message.useMessage();
  const today = new Date().toISOString().split("T")[0];
  
  console.log(typeof date);
  const dispatch = useDispatch();
  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetchTodo();
        console.log(response);
        dispatch(get({ response }));
        messageApi.open({
          type: "success",
          content: "Todays task fetched successfully",
        });
      } catch (error) {
        console.error("Error fetching data:", error);
        messageApi.open({
          type: "error",
          content: "Error fetching data",
        });
      }
    };

    fetchData();
  }, []);
  const todoData = useSelector((state) => {
    console.log(state);
    return state.todo.todo;
  });
  const loading = useSelector((state) => {
    console.log(state.todo.loading);
    return state.todo.loading;
  });

  const completedTask = async (id) => {
    try {
      const response = await completedTodo(id);
      dispatch(close(id));

      messageApi.open({
        type: "success",
        content: "Task completed successfully",
      });
    } catch (error) {
      console.log("error in closed task:", error);
      messageApi.open({
        type: "error",
        content: "Error in closed task",
      });
    }
  };
  
  const handleDelete = async (id) => {
    try {
      await deleteTodo(id);
      dispatch(remove(id));
      messageApi.open({
        type: "success",
        content: "Task deleted successfully",
      });
    } catch (error) {
      console.log("error in deleting task:", error);
      messageApi.open({
        type: "error",
        content: "Error in deleting task",
      });
    }
  };
  const handleAdd = async (data,date) => {
    try {
      const response = await addTodo(data, date);
      dispatch(add(response));
      messageApi.open({
        type: "success",
        content: "Task added successfully",
      });
    } catch (error) {
      console.log("error in adding task:", error);
      messageApi.open({
        type: "error",
        content: "Error in adding task",
      });
    }
  };
  const updateTasks = async (id,data,date) => {
    try {
      const response = await updateTaskDetails(id,data,date);
      dispatch(updated({data,date,id}));
      
    } catch (error) {
      console.log("error in closed task:", error);
    }
  };
  return (
    <>
      {" "}
      {contextHolder}
      <Space
        direction="vertical"
        style={{ marginLeft: "25vw", marginTop: "2vw", cursor: "pointer" }}
      >
        <h1 style={{ padding: "10px" }}>Today</h1>
        {loading ? (
          <Flex align="center" justify="center">
            <Spin size="large" />
          </Flex>
        ) : todoData.length > 0 ? (
          todoData.map((todo) =>
            todo.due && todo.due.date === today && !todo.is_complete ? (
              <>
                <div
                  key={todo.id}
                  style={{
                    width: "45vw",
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "20px",
                  }}
                >
                  <div style={{ cursor: "pointer", fontSize: "18px" }}>
                    <Checkbox
                      className="foo"
                      onChange={() => completedTask(todo.id)}
                      style={{ paddingRight: "20px" }}
                    />

                    {todo.content}
                    {todo.due ? ` - Due: ${todo.due.string}` : null}
                  </div>
                  <div>
<Update name={"task"} id={todo.id} addfunction={updateTasks}/>
                  <DeleteOutlined onClick={() => handleDelete(todo.id)} />
                </div></div>
                <hr style={{ opacity: 0.2 }} />
              </>
            ) : null
          )
        ) : (
          <div>No tasks found.</div>
        )}
        <Add
          name="task"
        
          addfunction={handleAdd}
        />
      </Space>
    </>
  );
}

export default Todo;
