import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, Link } from "react-router-dom";
import { Space, message, Spin, Flex, Checkbox, Badge } from "antd";
import {
  fetchsection,
  deletesection,
  addingsection,
  addTodoProject,
  deleteTodo,
  completedTodo,
  updateSections,
  updateTaskDetails
} from "../API/api";
import { get, remove, add,update as updateSection } from "./Store/Slicer/project";
import {
  add as addTask,
  remove as removeTask,
  close,
  updated

} from "./Store/Slicer/todo";
import { DeleteOutlined, MessageFilled } from "@ant-design/icons";
import Add from "./Add";
import "../App.css";
import ProjectList from "./ProjectList";
import Comments from "./Comments";
import Update from "./Update";
import { update  } from "./Store/Slicer/slidebarSlicer";
const count = parseInt(localStorage.getItem("count")) || 0;

function Projects() {
  const { id } = useParams();
  const [Commentcount, setCount] = useState(0);
  const [display, setDisplay] = useState(false);
  const dispatch = useDispatch();
  const [messageApi, contextHolder] = message.useMessage();
  const projectName = useSelector((state) =>
    state.projects.projects.find((project) => project.id === id)
  );
  const unnamed = useSelector((state) =>
    state.todo.todo.filter(
      (todo) => todo.section_id === null && todo.project_id === id
    )
  );
  const loading = useSelector((state) => state.projectsDet.loading);

  const fetchSections = async () => {
    try {
      const response = await fetchsection(id);
      dispatch(get({ response }));
      messageApi.open({
        type: "success",
        content: "Sections fetched successfully",
      });
    } catch (error) {
      console.error("Error fetching sections:", error);
      messageApi.open({
        type: "error",
        content: "Error fetching sections",
      });
    }
  };

  const handleDelete = async (sectionId) => {
    await deletesection(sectionId);
    console.log("Deleting section with id:", sectionId);
    dispatch(remove(sectionId));
  };

  useEffect(() => {
    fetchSections();
    localStorage.setItem("count", count + 1);
  }, [id]);

  const section = useSelector((state) => state.projectsDet.projectsDet);

  const handleAdd = async (data) => {
    try {
      const response = await addingsection(id, data);
      dispatch(add({ response }));
    } catch (error) {
      console.log("Error adding section", error);
    }
  };
  const handleAddtaskonProject = async (data, date) => {
    try {
      const response = await addTodoProject(data, date, id);
      console.log(response);
      dispatch(addTask(response));
    } catch (error) {
      console.log("error in posting todo in sections", error);
    }
  };
  const handleDeleteTask = async (id) => {
    try {
      await deleteTodo(id);
      dispatch(removeTask(id));
    } catch (error) {
      console.log("error in deleting task", error);
    }
  };
  const completedTask = async (id) => {
    try {
      const response = await completedTodo(id);
      dispatch(close(id));
      messageApi.open({
        type: "success",
        content: "Task completed successfully",
      });
    } catch (error) {
      console.log("error in closed task:", error);
      messageApi.open({
        type: "error",
        content: "Error in closed task",
      });
    }
  };
  const handleUpdateSection = async (id,data) =>{
    try {
      const response = await updateSections(id,data);
      dispatch(updateSection({id:id,response:response}))
      messageApi.open({
        type:'success',
        content: 'Section updated successfully',
      })
    } catch (error) {
      messageApi.open({
        type: 'error',
        content: 'Error in updating section',
      })
      console.log('error in updating section:', error);
    }
  }
  const updateTask = async (id,data,date) => {
    try {
      const response = await updateTaskDetails(id,data,date);
      dispatch(updated({data,date,id}));
      
    } catch (error) {
      console.log("error in closed task:", error);
    }
  };
  return (
    <>
      {contextHolder}
      {display ? (
        <Comments
          display={display}
          setDisplay={setDisplay}
          setCount={setCount}
          projectName={projectName.name}
          id={id}
        />
      ) : (
        <Badge count={Commentcount} size="large">
          <MessageFilled
            size="large"
            style={{ marginLeft: "80vw" }}
            onClick={() => setDisplay(true)}
          />
        </Badge>
      )}

      <Space
        direction="vertical"
        style={{ marginLeft: "25vw", marginTop: "2vw", cursor: "pointer" }}
      >
        {projectName && <h1 style={{ padding: "10px" }}>{projectName.name}</h1>}
        {loading ? (
          <Flex align="center" justify="center">
            <Spin size="large" />
          </Flex>
        ) : (
          <Space direction="vertical">
            <>
              {unnamed.map(
                (todo) =>
                  !todo.is_complete && (
                    <Flex align="center" justify="space-between" key={todo.id}>
                      <div style={{ cursor: "pointer", fontSize: "18px" }}>
                        <Checkbox onChange={() => completedTask(todo.id)} style={{padding:'10px'}}/>
                      </div>
                      <div
                        style={{
                          width: "45vw",
                          display: "flex",
                          justifyContent: "space-between",
                          padding: "10px ",
                          marginTop: "10px",
                        }}
                      >
                        <div style={{ cursor: "pointer", fontSize: "18px" }}>
                          {todo.content}
                        </div>
                        <div>
                        <Update name={"task"} id={todo.id} addfunction={updateTask}/>
                        <DeleteOutlined
                          onClick={() => handleDeleteTask(todo.id)}
                        /></div>
                      </div>
                    </Flex>
                  )
              )}
              <Add name="task" addfunction={handleAddtaskonProject} />
            </>
            {section.map((sec) => (
              <Space direction="vertical" key={sec.id}>
                <div
                  style={{
                    width: "45vw",
                    display: "flex",
                    justifyContent: "space-between",
                    padding: "10px",
                  }}
                >
                  <div style={{ cursor: "pointer", fontSize: "18px" }}>
                    {sec.name}
                  </div>
                  <div>
                    
                    <Update id={sec.id} name={'section'} addfunction={handleUpdateSection}/>
                    <DeleteOutlined onClick={() => handleDelete(sec.id)} />
                  </div>
                </div>
                <ProjectList id={sec.id} />
              </Space>
            ))}
          </Space>
        )}
        <Space direction="horizontal" width="45vw" className="hoverHandle">
          <hr className="custom-hr" />
          <Add name="section" addfunction={handleAdd} />
          <hr className="custom-hr" />
        </Space>
      </Space>
    </>
  );
}

export default Projects;
