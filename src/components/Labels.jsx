import React, { useState, useEffect } from "react";
import { getLabels, deleteLabels, updatedLabels, addLabels } from "../API/api";
import { Card, Space, Spin } from "antd";
import { DeleteOutlined, TagOutlined, TagsOutlined } from "@ant-design/icons";
import { get,add,remove,update } from "./Store/Slicer/labelSlicer";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import Update from "./Update";
import Add from "./Add";
function Labels() {
  //const [labels, setLabels] = useState([]);
  const dispatch = useDispatch();
  const labels= useSelector(state=>state.labels.labels);
 const loading = useSelector(state=>state.labels.loading);

  useEffect(() => {
    const fetchLabels = async () => {
      try {
        const response = await getLabels();
        console.log(response);
     dispatch(get({response}))
       
      } catch (error) {
        console.log("Error in fetching labels:", error);
      }
    };
    fetchLabels();
  }, []);
  const handleAdd = async (data) =>{
try {
    const response = await addLabels(data);
    dispatch(add({response}))
} catch (error) {
    console.log('error adding labels:', error);
}
  }
  const handleUpdate= async (id,data) => {
    try {
        const response = await updatedLabels(id,data);
        console.log(response);
        dispatch(update({id,response}))
    } catch (error) {
        console.log('error updating labels:', error);
    }
  }
const handleDelete = async (id) => {
    try {
        await deleteLabels(id);
        dispatch(remove(id));
    } catch (error) {
        console.log('error in deleting labels:', error);
    }
}
  return (
    <Space
      direction="vertical"
      style={{ marginLeft: "25vw", marginTop: "2vw", cursor: "pointer" }}
    >
      <Space style={{ fontSize: "20px" }}>
        <div>
          <TagsOutlined style={{ padding: "10px" }} />
          Filters
        </div>
      </Space>
      <Space style={{ padding: "15px" }} direction="vertical">
        {loading ? (
          <div><Spin/></div>
        ) : labels.length > 0 ? (
          labels.map((label, index) => (
            <div style={{ fontSize: "18px", display: "flex",flexDirection:'horizontal' }}>
              <TagOutlined style={{marginBottom:'10px'}}/>
              <div style={{ paddingLeft: "15px",width:'40vw' }} key={index}>
                {label.name}
              </div>
              <div style={{display:'flex',}}>
                <Update name={'labels'} id={label.id} addfunction={handleUpdate} />
                <DeleteOutlined onClick={()=>handleDelete(label.id)}/>
              </div>
            </div>

          ))
    
        ) : (
          <div>No labels found</div>
        )}
      
      </Space> 
       <Add name={"label"} addfunction={handleAdd} />
    </Space>
  );
}

export default Labels;
