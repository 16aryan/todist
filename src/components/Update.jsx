import React, { useState } from "react";
import { Button, Form, Input, Modal, DatePicker } from "antd";
import { EditOutlined } from "@ant-design/icons";

const Update = ({ name, addfunction, id }) => {
  const [open, setOpen] = useState(false);
  const [formData, setFormData] = useState("");
  const [date, setFormDate] = useState(null);

  const handleCancel = () => {
    setOpen(false);
  };
  const onFinish = (values) => {
    setOpen(false);
  
    if (name === "task") {
      const dateObject = date.$d;
      const options = { day: "numeric", month: "short" };
      const dateString = dateObject.toLocaleDateString("en", options);
  
      addfunction(id, formData, dateString);
      setFormData("")
    } else {
      addfunction(id, formData);
      setFormData("")
    }
  
   values.name=''
   console.log(values);
   console.log(formData);
  };
  
  return (
    <>
      <Button type="text" onClick={() => setOpen(true)}>
        <EditOutlined />
      </Button>
      <Modal
        title={`Update ${name}`}
        open={open}
        onCancel={handleCancel}
        footer={null}
       
      >
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          onFinish={onFinish}
         
        >
          <Form.Item
            label={`${name} name`}
            name="name"
            rules={[{ required: true, message: `Please input ${name} name!` }]}
          >
            <Input
              value={formData}
              onChange={(e) => setFormData(e.target.value)}
            />
          </Form.Item>
          {name === "task" ? (
            <Form.Item
              label="Select Date"
              name="date"
              rules={[{ required: true, message: "Please select a date!" }]}
            >
              <DatePicker onChange={(date) => setFormDate(date)} />
            </Form.Item>
          ) : null}
          <Button
            type="primary"
            htmlType="submit"
            style={{ backgroundColor: "rgb(170,44,23)" }}
          >
            Submit
          </Button>
        </Form>
      </Modal>
    </>
  );
};

export default Update;
