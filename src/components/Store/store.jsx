import { configureStore } from "@reduxjs/toolkit";
import slidebarSlicer from "./Slicer/slidebarSlicer";
import todo from "./Slicer/todo";
import projectSlicer from "./Slicer/project";
import commentSlicer from "./Slicer/comments";
import labelSlicer from "./Slicer/labelSlicer";
export const store = configureStore({
  reducer: {
    projects:slidebarSlicer.reducer,
    todo:todo.reducer,
    projectsDet:projectSlicer.reducer,
    comments:commentSlicer.reducer,
    labels:labelSlicer.reducer,
  },
});
