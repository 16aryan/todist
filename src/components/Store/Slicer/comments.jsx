import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  comments:[],
  loading:true
};

 const commentSlicer = createSlice({
  name: "comments",
  initialState,
  reducers: { 
    get: (state, action) => {
        state.comments = action.payload.response;
        state.loading = false;
    },
    add: (state, action) => {
      return {
        ...state,
        comments: [...state.comments, action.payload.response]
      };
    },
    remove: (state, action) => {
      state.comments = state.comments.filter((comment) => comment.id !== action.payload);
    },
 
  },
});

export const { get, add, remove } = commentSlicer.actions;

export default commentSlicer;
