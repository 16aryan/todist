import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  projects: [],
  loading: true,
};

const slidebarSlicer = createSlice({
  name: "project",
  initialState,
  reducers: {
    get: (state, action) => {
      console.log(action.payload);
      state.projects = action.payload.response;
      state.loading = false;
      console.log(state.projects);
    },
    add: (state, action) => {
      return {
        ...state,
        projects: [...state.projects, action.payload],
      };
    },
    remove: (state, action) => {
      state.projects = state.projects.filter(
        (project) => project.id !== action.payload
      );
    },
    saveId: (state, action) => {
      state.saveId = action.payload;
    },
    update: (state, action) => {
      console.log(action.payload);
      state.projects = state.projects.map((project) =>
        project.id === action.payload.id ? action.payload.response : project
      );
    },
  },
});

export const { get, add, remove, saveId ,update} = slidebarSlicer.actions;

export default slidebarSlicer;
