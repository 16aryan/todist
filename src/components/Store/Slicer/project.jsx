import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  projectsDet:[],
  loading:true
};

 const projectSlicer = createSlice({
  name: "projectDetails",
  initialState,
  reducers: { 
    get: (state, action) => {
        state.projectsDet = action.payload.response;
        state.loading = false;
    },
    add: (state, action) => {
      return {
        ...state,
        projectsDet: [...state.projectsDet, action.payload.response]
      };
    },
    remove: (state, action) => {
      state.projectsDet = state.projectsDet.filter((project) => project.id !== action.payload);
    },
    update: (state, action) => {
      state.projectsDet = state.projectsDet.map((project) =>
        project.id === action.payload.id ? action.payload.response : project
      );
    }
 
  },
});

export const { get, add, remove,saveId,update } = projectSlicer.actions;

export default projectSlicer;
