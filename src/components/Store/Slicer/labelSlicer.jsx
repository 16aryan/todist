import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  labels:[],
  loading:true
};

 const labelSlicer = createSlice({
  name: "labels",
  initialState,
  reducers: { 
    get: (state, action) => {
        state.labels = action.payload.response;
        state.loading = false;
    },
    add: (state, action) => {
      return {
        ...state,
        labels: [...state.labels, action.payload.response]
      };
    },
    remove: (state, action) => {
      state.labels = state.labels.filter((label) => label.id !== action.payload);
    },
 update: (state, action) => {
    state.labels = state.labels.map((label) =>
      label.id === action.payload.id ? action.payload.response : label
    );
  },
 
  },
});

export const { get, add, remove,update } = labelSlicer.actions;

export default labelSlicer;
