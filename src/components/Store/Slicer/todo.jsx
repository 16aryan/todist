import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  todo:[],
  loading:true,
};

 const todo = createSlice({
  name: "todo",
  initialState,
  reducers: { 
    get: (state, action) => {
   console.log(action.payload);
      state.todo=(action.payload.response);
      state.loading=false;
   console.log(state.projects);
    },
    add: (state, action) => {
      return {
        ...state,
        todo: [...state.todo, action.payload]
      };
    },
    remove: (state, action) => {
      state.todo = state.todo.filter((project) => project.id !== action.payload);
    },
    close: (state, action) => {
      state.todo = state.todo.map((project) =>
        project.id === action.payload ? { ...project, is_complete: true } : project
      );
    },
    updated:(state,action) => {
      state.todo = state.todo.map((project) =>
        project.id === action.payload.id ? { ...project, content: action.payload.data, due_string: action.payload.date } : project
      );
    }

  },
});

export const { get, add, remove,close,updated } = todo.actions;

export default todo;
