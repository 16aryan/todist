import React, { useEffect } from "react";
import { Button, Modal, Space, Spin } from "antd";
import { getComment } from "../API/api";
import { useDispatch, useSelector } from "react-redux";
import { get } from "./Store/Slicer/comments";

function Comments({ display, setDisplay, projectName, id }) {
  const dispatch = useDispatch();

  useEffect(() => {
    const fetchedComments = async () => {
      try {
        const response = await getComment(id);
        dispatch(get({ response }));
      } catch (error) {
        console.log("error in fetching comments", error);
      }
    };

    if (display) {
      fetchedComments();
    }
  }, [display, id, dispatch]);

  const fetchedComment = useSelector((state) => state.comments.comments);
  const loading = useSelector((state) => state.comments.loading);

  return (
    <Modal
      title={projectName}
      visible={display}
      onCancel={() => setDisplay(false)}
      footer={[
        <Button key="close" onClick={() => setDisplay(false)}>
          Close
        </Button>
      ]}
    >
      {loading ? (
        <Spin />
      ) : (
        <Space direction="vertical">
          {fetchedComment.map((comment) => (
            <div key={comment.id}>{comment.text}</div>
          ))}
        </Space>
      )}
    </Modal>
  );
}

export default Comments;
