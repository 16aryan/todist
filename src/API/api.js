import axios from "axios";
const apiToken = import.meta.env.VITE_API_TOKEN;
import { nanoid } from "nanoid";

export const fetchProject = async () => {
  try {
    const response = await axios.get(
      "https://api.todoist.com/rest/v2/projects",
      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    const data = response.data;
    return data;
  } catch (error) {
    console.log("error fetching project", error);
  }
};

export const addProject = async (sendData) => {
  try {
    const response = await axios.post(
      "https://api.todoist.com/rest/v2/projects",
      { name: sendData },
      {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": nanoid(),
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error adding project:", error);
    throw error;
  }
};

export const deleteProject = async (id) => {
  try {
    console.log("info deleting project");
    await axios.delete(`https://api.todoist.com/rest/v2/projects/${id}`, {
      headers: {
        "X-Request-Id": nanoid(),
        Authorization: `Bearer ${apiToken}`,
      },
    });
  } catch (error) {
    console.log("error deleting project", error);
  }
};

export const fetchTodo = async () => {
  try {
    const response = await axios.get("https://api.todoist.com/rest/v2/tasks", {
      headers: {
        Authorization: `Bearer ${apiToken}`,
      },
    });

    return response.data;
  } catch (error) {
    console.log("error fetching tasks", error);
  }
};

export const completedTodo = async (id) => {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/tasks/${id}/close`,
      {},
      {
        headers: {
          "X-Request-Id": nanoid(),
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );

    console.log("Task completed successfully:", response.data);

    return response.data;
  } catch (error) {
    console.error("Error completing task:", error);
    throw error;
  }
};

export const deleteTodo = async (id) => {
  try {
    const response = await axios.delete(
      `https://api.todoist.com/rest/v2/tasks/${id}`,

      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );

    console.log("Task deleted successfully:", response.data);

    return response.data;
  } catch (error) {
    console.error("Error completing task:", error);
    throw error;
  }
};
export const addTodo = async (data, today) => {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/tasks`,
      {
        content: data,
        due_string: today,
        due_lang: "en",
        priority: "4",
      },
      {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": nanoid(),
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log("error in adding of task:", error);
  }
};
export const addTodoProject = async (data, today, id) => {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/tasks`,
      {
        content: data,
        project_id: id,
        due_string: today,
        due_lang: "en",
        priority: "4",
      },
      {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": nanoid(),
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log("error in adding of task:", error);
  }
};
export const addTodoSection = async (data, today, id) => {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/tasks`,
      {
        content: data,
        section_id: id,
        due_string: today,
        due_lang: "en",
        priority: "4",
      },
      {
        headers: {
          "Content-Type": "application/json",
          "X-Request-Id": nanoid(),
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log("error in adding of task:", error);
  }
};
export async function fetchsection(projectId) {
  try {
    const response = await axios.get(
      "https://api.todoist.com/rest/v2/sections",
      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
        params: {
          project_id: projectId,
        },
      }
    );

    return response.data;
  } catch (error) {
    console.error("Error fetching sections:", error.message);
  }
}
export async function deletesection(projectId) {
  try {
    await axios.delete(
      `https://api.todoist.com/rest/v2/sections/${projectId}`,
      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
  } catch (error) {
    console.log("error deleting", error);
  }
}
export async function addingsection(projectId, data) {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/sections`,
      {
        project_id: projectId,
        name: data,
      },
      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    console.error("Error adding section:", error.message);
  }
}
export const getComment = async (id) => {
  try {
    const response = await axios.get(
      `https://api.todoist.com/rest/v2/comments?project_id=${id}`,
      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log("Error", error);
  }
};

export async function updateTaskDetails(taskId, data, date) {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/tasks/${taskId}`,
      {
        content: data,
        due_string: date,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${apiToken}`,
          "X-Request-Id": nanoid(),
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log("Error updating task details:", error);
  }
}
export async function updateProject(projectId, data) {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/projects/${projectId}`,
      {
        name: data,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${apiToken}`,
          "X-Request-Id": nanoid(),
        },
      }
    );
    return response.data;
  } catch (error) {
    console.log("error updating project details:", error);
  }
}

export async function updateSections(sectionId, data) {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/sections/${sectionId}`,
      {
        name: data,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${apiToken}`,
          "X-Request-Id": nanoid(),
        },
      }
    );

    console.log("Section updated successfully:", response.data);
    return response.data;
  } catch (error) {
    console.log("error updating sections:", error);
  }
}

export const getLabels = async () => {
  try {
    const response = await axios.get("https://api.todoist.com/rest/v2/labels", {
      headers: {
        Authorization: `Bearer ${apiToken}`,
      },
    });

    return response.data;
  } catch (error) {
    console.error("Error fetching labels:", error.message);
  }
};

export const deleteLabels = async (id) => {
  try {
    const response = await axios.delete(
      `https://api.todoist.com/rest/v2/labels/${id}`,
      {
        headers: {
          Authorization: `Bearer ${apiToken}`,
        },
      }
    );
  } catch (error) {
    console.log("error deleting labels:", error);
  }
};
export const updatedLabels = async (id,data) => {
try {
  const response= await axios.post(`https://api.todoist.com/rest/v2/labels/${id}`,{
    name:data
  },{
    headers:{
      "Content-Type":"application/json",
      Authorization:`Bearer ${apiToken}`,
      "X-Request-Id":nanoid()
    }
  })
return response.data;
  
} catch (error) {
  console.log('error updating labels:', error);
}

};

export const addLabels = async ( data) => {
  try {
    const response = await axios.post(
      `https://api.todoist.com/rest/v2/labels`,
      {
        name: data,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${apiToken}`,
          "X-Request-Id": nanoid(),
        },
      }
    );

    console.log("Label added successfully:", response.data);
    return response.data;
  } catch (error) {
    console.log("Error in adding labels:", error);
    throw error;
  }
};
